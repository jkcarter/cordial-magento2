# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [2.4.5] - 2022-10-03

### Added

- Beta support of Magento 2.4.5

### Fixed

- Send cart events only on actual cart update

## [2.4.3-p14] - 2022-09-26

### Fixed

- Disabled sending of cart events on checkout pages

## [2.4.3-p13] - 2022-07-15

### Added

- Provided a new setting in module config: 'Enable Wishlist Sync'
- Added new 'returned_products' property to TrackJs 'search' event, which contains TOP10 product SKUs from search result

### Fixed

- Customer sync no longer fails on thrown exception while fetching wishlist data

## [2.4.3-p12] - 2022-06-09

### Fixed

- Issuing clear cart request on removing last cart item

## [2.4.3-p11] - 2022-02-09

### Fixed

- Reset `paramsPost` between subsequent requests

## [2.4.3-p10] - 2022-01-26

### Added

- Fire `cart` event on adding items to cart
- Sync orders from users that use guest checkout process

## [2.4.3-p9] - 2021-11-26

### Changed

- Improve handling of TrackJsv2 events to be compatible with Cordial Web Forms

## [2.4.3-p8] - 2021-11-15

### Changed

- Switch Cordial API to v2
- Switch Cordial API to https

## [2.4.3-p7] - 2021-11-10

### Changed

- Rename 'contactData' JS const to 'contactDataCrdl' to prevent name intersection with WebForms JS listener

## [2.4.3-p6] - 2021-10-26

### Changed

- Update Cordial logo

## [2.4.3-p5] - 2021-10-12

### Changed

- Send unsubscribe request as PUT instead of POST

## [2.4.3-p4] - 2021-10-11

### Added

- Adapt package for publishing to packagist.org

## [2.4.3-p3] - 2021-09-22

### Added

- Add identifyBy parameter to API Call

## [2.4.3-p2] - 2021-09-20

### Added

- Allow setting custom Cordial API URL in configuration

## [2.4.3-p1] - 2021-09-16

### Added

- Beta support of Magento 2.4.3
