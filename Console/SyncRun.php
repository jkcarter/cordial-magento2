<?php

namespace Cordial\Sync\Console;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SyncRun extends Command {
	protected function configure() {
		   $this->setName('cordial:syncrun');
		   $this->setDescription('Run Sync process');
		   
		   parent::configure();
	}

	protected function execute(InputInterface $input, OutputInterface $output) {
        $this->state->setAreaCode(\Magento\Framework\App\Area::AREA_FRONTEND);
		$output->writeln("Sync in progress...");

        /* // debug run
        //$this->obs->scheduledGenerateSitemaps();
        $order = $this->_objectManager->create('Magento\Sales\Model\Order')->load(18);
        $quote = $this->_objectManager->create('Magento\Quote\Model\Quote')->load($order->getQuoteId());
        $this->_objectManager->get(\Magento\Checkout\Helper\Data::class)
                ->sendPaymentFailedEmail($quote, 'payment_failed_emulated');*/


	    $this->cronjob->execute();
	}

	public function __construct(
        \Magento\Framework\App\State $state,
        \Cordial\Sync\Cron\Cronjob $cronjob,
		\Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Sitemap\Model\Observer $obs
    ) {
        $this->obs = $obs;
        $this->_objectManager = $objectManager;

        $this->cronjob	= $cronjob;
        $this->state    = $state;
        parent::__construct();
    }
}

